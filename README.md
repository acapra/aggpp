# AgGpp

Simulation of the ALPHA-g rTPC using Garfield++ 

## Basic Requirements:

* [GNU C Compiler](https://gcc.gnu.org/gcc-8/) **version >=8**



* [ROOT][rootlink] 

  [rootlink]: https://root.cern.ch/



* [Garfield++](http://garfieldpp.web.cern.ch/garfieldpp/ "Garfield++ at CERN")

```
git clone https://gitlab.cern.ch/garfield/garfieldpp.git
```

Follow [these][gppinstall] instructions to install Garfield++

[gppinstall]: http://garfieldpp.web.cern.ch/garfieldpp/getting-started/


