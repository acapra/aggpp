#include <iostream>
#include <string>

#include <TCanvas.h>
#include <TROOT.h>
#include <TApplication.h>

#include "Garfield/ComponentAnalyticField.hh"
#include "Garfield/MediumMagboltz.hh"
#include "Garfield/Sensor.hh"
#include "Garfield/DriftLineRKF.hh"
#include "Garfield/AvalancheMC.hh"
#include "Garfield/ViewSignal.hh"
#include "Garfield/FundamentalConstants.hh"

using namespace Garfield;

int main(int argc, char * argv[])  { 

  TApplication app("app", &argc, argv);
 
  // Make a gas medium.
  MediumMagboltz gas;
  gas.LoadGasFile("ar_70_co2_30_725Torr_20E200000_4B1.10.gas");
  const std::string path = getenv("GARFIELD_HOME");
  gas.LoadIonMobility(path + "/Data/IonMobility_Ar+_Ar.txt");

  // Define the cell layout.
  constexpr double lZ = 115.2 / 16.;
  ComponentAnalyticField cmp;
  Sensor sensor;
  sensor.AddComponent(&cmp);

  cmp.SetMedium(&gas);
  // cmp.SetMagneticField(0, 0, 1);
  cmp.SetPolarCoordinates();
  // Outer wall.
  constexpr double rRO = 19.0;
  // cmp.AddPlaneR(rRO, 0, "ro");
  // cmp.AddReadout("ro");
  // sensor.AddElectrode(&cmp, "ro");
  // Inner wall.
  constexpr double rCathode = 10.925;
  cmp.AddPlaneR(rCathode, -4000., "c");
  constexpr int nWires = 256;
  // Phi periodicity.
  const double sphi = 360. / double(nWires);
  std::cout << "Phi periodicity: " << sphi << std::endl;

  // Field wires.
  // Radius [cm]
  constexpr double rFW = 17.4;
  // Diameter [cm]
  constexpr double dFW = 152.e-4;
  // Potential [V]
  constexpr double vFW = -99.;

  // Anode wires.
  constexpr double rAW = 18.2;
  constexpr double dAW = 30.e-4;
  constexpr double vAW = 3200.;

  // Add the wires.
  for (int i = 0; i < nWires; ++i) { 
    // Field wires.
    cmp.AddWire(rFW, i * sphi, dFW, vFW, "f", 2 * lZ);
    // Anode wires.
    auto ename = std::string(TString::Format("a%03d", i).Data());
    cmp.AddWire(rAW, (i + 0.5) * sphi, dAW, vAW, ename, 2 * lZ);
    if (i < nWires / 4) {
      cmp.AddReadout(ename);
      sensor.AddElectrode(&cmp, ename);
    }
  }

  // Pads.
  constexpr double gap = rRO - rAW;
  constexpr int nSecs = 32;
  constexpr double pitchPhi = TwoPi / nSecs;
  constexpr double pitchZ = 0.4;
  constexpr int nRows = int(2 * lZ / pitchZ);
  std::cout << "Number of pad rows: " << nRows << std::endl;
  for (int j = 0; j < nRows; ++j) {
    const double z0 = -lZ + j * pitchZ;
    const double z1 = z0 + pitchZ;
    std::string row = std::string(TString::Format("%03d", j).Data());
    for (int i = 0; i < nSecs; ++i) {
      std::string sec = std::string(TString::Format("%02d", i).Data());
      const double phi0 = i * pitchPhi * RadToDegree; 
      const double phi1 = phi0 + pitchPhi * RadToDegree;
      std::string ename = "pad" + row + "_" + sec;
      cmp.AddPixelOnPlaneR(rRO, phi0, phi1, z0, z1, ename, gap);
      cmp.AddReadout(ename);
      sensor.AddElectrode(&cmp, ename);
    }
  }
  const double tStart = 800.;
  //  const int nSteps = 411;
  const int nSteps = 70;
  const double tStep = 16.;
  sensor.SetTimeWindow(tStart, tStep, nSteps);

  DriftLineRKF drift;
  drift.SetSensor(&sensor);

  // Electron initial point
  // const double phi0 = 1.5 * pitchPhi;
  const double phi0 = 12.5 * sphi * DegreeToRad;
  const double r0 = rFW + 2 * dFW;
  const double x0 = r0 * cos(phi0);
  const double y0 = r0 * sin(phi0);
  const double z0 = 0.5 * pitchZ;
  const double t0 = tStart+200.; // time shift
  if (!drift.DriftElectron(x0, y0, z0, t0)) {
    std::cerr << "Electron drift line calculation FAILED.\n";
    return 0;
  }
  double x1, y1, z1, t1;
  int status;
  drift.GetEndPoint(x1, y1, z1, t1, status);
  double phi1 = atan2(y1, x1);
  if (phi1 < 0.) phi1 += TwoPi;
  double r1 = sqrt(x1 * x1 + y1 * y1);
  std::cout << "Electron ends up at r = " << r1 << " cm.\n";
  int aw = int(phi1*RadToDegree/sphi-0.5);
  std::cout << "Electron ends up on wire " << aw << " (phi="<<phi1<<")\n";
  // Find pads.
  int sec = int(phi1 / pitchPhi);
  int row = int((z1 + lZ) / pitchZ);
  std::cout << "Hit pad: (" << sec << "," << row << ")\n";
  double ne, ni;
  drift.GetAvalancheSize(ne, ni);
  std::cout << "AvalancheSize: ne=" << ne << " ni=" << ni << std::endl;
  drift.SetIonSignalScalingFactor(ni);
  drift.DriftIon(x1, y1, z1, t1);
  double x2, y2, z2, t2;
  drift.GetEndPoint(x2, y2, z2, t2, status);
  double r2 = sqrt(x2 * x2 + y2 * y2);
  std::cout << "Ion ends up at r = " << r2 << " cm.\n";
  std::cout << "Done.\n";

  // Plot the induced current.
  TCanvas c1("c1","", 2700, 2700);
  c1.Divide(3, 3);
  ViewSignal signalView;
  signalView.SetSensor(&sensor);
  int pad = 1;
  for (int i = sec - 1; i <= sec + 1; ++i) {
    for (int j = row - 1; j <= row + 1; ++j) {
      signalView.SetCanvas((TPad*)c1.cd(pad));
      ++pad;
      if (i < 0 || j < 0) continue;
      std::string ename = "pad";
      ename += std::string(TString::Format("%03d", j).Data());
      ename += "_" + std::string(TString::Format("%02d", i).Data());
      std::cout << "Plotting " << ename << "\n";
      signalView.PlotSignal(ename, true, true, true);
    }
  }
  c1.SetTitle("Pad Induced Current");
  c1.SaveAs("pads_induced_current.pdf");

  // TCanvas c2("cro","",900,900);
  // signalView.SetCanvas(&c2);
  // signalView.PlotSignal("ro");
  // c2.SetTitle("ro");

  TString wname = TString::Format("a%03d", aw);
  TString cname  = "c";
  TCanvas c3(cname+wname,"",900,900);
  signalView.SetCanvas(&c3);
  signalView.PlotSignal(wname.Data());
  c3.SetTitle(wname);
  c3.SaveAs(wname+"_induced_current.pdf");

  app.Run(true);

  return 0;
}
