INCDIR = $(GARFIELD_HOME)/install/include/
LIBDIR = $(GARFIELD_HOME)/install/lib

CFLAGS += -O3 -g -Wall -Wextra -Wno-long-long -Wuninitialized `root-config --cflags` -fno-common -c -I$(INCDIR) -I./include

LDFLAGS = `root-config --glibs` -lGeom -lgfortran -lm -L$(LIBDIR) -lGarfield -g

SOURCES := $(wildcard *.cpp)
OBJECTS := $(patsubst %.cpp,obj/%.o,$(SOURCES))
BINARIES := signal.exe


all:: odir
all:: $(OBJECTS)
all:: $(BINARIES)
#all:: ddir

odir:
	mkdir -p obj

$(BINARIES): %.exe: obj/%.o
	$(CXX) -o $@ $< $(LDFLAGS)

$(OBJECTS): obj/%.o: %.cpp
	$(CXX) -o $@ $(CFLAGS) -c $<

clean:
	rm -f $(BINARIES)
	rm -fr ./obj

ddir:
	rm -fr ./obj
